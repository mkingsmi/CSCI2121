
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;


/**
 *
 * @author noop
 */
public class EventSerializer {
    public static void main(String[] args) {
        Event party = new Event("Mardi Gras", "2016-02-05 16:00", "New Orleans", "Que les bons temps roulent");
        Event test = new Event("CSCI Test", "2016-02-05 16:00", "UNO", "Tests are no fun.");
        
        try{
            ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("calendar.ser"));
            output.writeObject(party);
            output.writeObject(test);
            output.close();
        }
        catch(IOException e){
            System.out.println("Error while writing");
        }
        
        try{
            ObjectInputStream input = new ObjectInputStream(new FileInputStream("calendar.ser"));
            String stringOne;
            String stringTwo;
            
            Event one = (Event) input.readObject();
            Event two = (Event) input.readObject();
            
            stringOne = one.toString();
            stringTwo = two.toString();
            
            System.out.println(stringOne +" \n" + stringTwo);
        }
        
        catch(IOException e){
            System.out.println("Error while writing");
        }
        
        catch(ClassNotFoundException e){
            System.out.println("Class not found.");
        }
        

    }
}
