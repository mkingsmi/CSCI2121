/*
 *  This part is complete, minus descriptions for javadoc items
 */

/**
 *
 * @author Mark Kingsmill
 */
    public class ComplexNumber{
    // The instance variables a and b representing
    // the real parts of the complex number
    private float a;
    private float b;

    /**
     *
     * @param _a
     * @param _b
     */
    public ComplexNumber(float _a, float _b){
        
        this.a = _a;
        this.b = _b;
        
    }

    /**
     *
     * @param otherNumber
     * @return
     */
    public ComplexNumber add(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = this.a + otherNumber.getA();
        float newB = this.b + otherNumber.getB();
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }
    //...
    /* You will fill in the rest of the methods */
    //...

    /**
     *
     * @param otherNumber
     * @return
     */
    public ComplexNumber subtract(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = a - otherNumber.getA();
        float newB = b - otherNumber.getB();
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;    
    }
    
    /**
     *
     * @param otherNumber
     * @return
     */
    public ComplexNumber multiply(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = (this.a * otherNumber.getA()) - (this.b * (otherNumber.getB()));
        float newB = (this.b * otherNumber.getA() + this.a * otherNumber.getB());
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;    
    }
    
    /**
     *
     * @param otherNumber
     * @return
     */
    public ComplexNumber divide(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float commonDenominator = (otherNumber.getA() * otherNumber.getA() + otherNumber.getB()*otherNumber.getB());  
        float newA = (this.a * otherNumber.getA() + this.b * otherNumber.getB()) / commonDenominator;
        float newB = (this.b * otherNumber.getA() - (this.a * otherNumber.getB())) / commonDenominator;
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;    
    }
    private float getA() {
        return a;
    }

    private float getB() {
        return b;    
    }
    
}