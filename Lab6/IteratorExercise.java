import java.util.*;

/**
 *
 * @author Mark Kingsmill
 */
public class IteratorExercise {
    
    public static void main(String[] args){
        
        Integer[] array = {3,4,18,54,33,87,99,109,72,8,5};
        LinkedList<Integer> list = new LinkedList<>(Arrays.asList(array));
        
        ListIterator<Integer> it = list.listIterator();
        
        System.out.println("Removed Elements:");
        while (it.hasNext()){
            int next = it.next();
            if (next % 2 == 0){
                it.remove();
                System.out.println(next);
            }
            
        }
        while(it.hasPrevious()){         
            it.previous();
        }
        
        System.out.println("Remaining Elements:");
        while(it.hasNext()){
            System.out.println(it.next());
        }
        
        
    } // end of main
    
} // end of class
