import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.junit.Before;

public class TestString {
    private String letters;
    private String numbers;
    private String empty;
    private String simpleSentence;
    private String complexSentence;
    
    @Before
    public void setup() {
        letters = new String("abc");
        numbers = new String("123");
        empty = new String("");
        simpleSentence = new String("This is a sentence. \n");
        complexSentence = new String("%Punctuation!, Can cr\te8 <problems.$");
        
    }
    
    @Test
    public void testLength() {
        assertTrue(letters.length() == 3);
        assertEquals(numbers.length(), letters.length());
        assertTrue(empty.length() == 0);
        assertEquals(simpleSentence.length(), 21);
        assertTrue(complexSentence.length() == 36);
    
    }
    
}