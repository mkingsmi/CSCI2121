import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.junit.Before;

public class FractionTest {

    private Fraction fractionOne;
    private Fraction fractionTwo;
    private Fraction fractionThree;
    private Fraction fractionFour;
    private Fraction fractionFive;
    private Fraction fractionSix;
    
    @Before
    public void Setup() {
        fractionOne = new Fraction(1, 2);
        fractionTwo = new Fraction(3, 4);
        fractionThree = new Fraction(5, 6);
        fractionFour = new Fraction(7, 8);
        fractionFive = new Fraction(9, 10);
        fractionSix = new Fraction(11, 12);
    }
    @Test
    public void testFractionAdd() {
        
        Fraction myFrac = fractionOne.add(fractionTwo);
        assertTrue(myFrac.getNumerator() == 10 && myFrac.getDenominator() == 8);
        myFrac = fractionTwo.add(fractionThree); 
        assertTrue(myFrac.getNumerator() == 38 & myFrac.getDenominator() == 24);
        myFrac = fractionThree.add(fractionFour);
        assertTrue(myFrac.getNumerator() == 82 && myFrac.getDenominator() == 48);
    
    }
        
    

}   
    